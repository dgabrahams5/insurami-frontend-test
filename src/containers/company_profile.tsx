import React from "react";
import { useState, useEffect } from 'react';
import { List, Avatar } from 'antd';
import '../App.css';

export type CompanyProfileProps = {
  companyProfile: any;
}

export interface companyOfficers {
  items: Array<object>;
}

export interface companyOfficerItem {
  name: string;
}

export interface companyOfficerListItem {
  title: string;
}

const CompanyProfile: React.FC<CompanyProfileProps> = ({
  companyProfile
}) => {

  const [officers, setOfficers] = useState<Array<companyOfficerListItem>>([{
    "title": ""
  }]);
  const [shareholders, setShareholders] = useState<Array<companyOfficerListItem>>([{
    "title": ""
  }]);
  const [showShareholders, setShowShareholders] = useState<boolean>(false);
  const [showDirectors, setShowDirectors] = useState<boolean>(false);

  useEffect(() => {
    if (!showDirectors) {
      const url = 'https://api.company-information.service.gov.uk/company/'+ companyProfile.company_number +'/officers';
      getAPI(url, 'getOfficers');
    }
    if (!showShareholders) {
      const url = 'https://api.company-information.service.gov.uk/company/'+ companyProfile.company_number +'/persons-with-significant-control';
      getAPI(url, 'getShareholders');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // for api here I pass in the url, amnd function to use to set the results
  const getAPI = (url: string, result: string) => {
    let username = "05b3fea5-3cec-4f66-9e03-9e78a18aacf6";
    let password = "";

    let headers = {
      'Authorization': 'Basic ' + btoa(username + ":" + password),
      'Accept': 'text/html',
    }

    fetch(url, {
      headers: headers,
    })
    .then((res)=>{ 
            if(res.ok) return res.json(); 
            else throw new Error("Status code error :" + res.status) 
    })
    .then((data) => {
      switch(result) {
        case "getOfficers":
          getOfficers(data);
          break;
        case "getShareholders":
          getShareholders(data);
          break;
      }
    })
    .catch(error=>console.log(error))
  };

  function getOfficers(data: companyOfficers) {
    let list: Array<companyOfficerListItem> = [];

    if (data.items.length >=1) {
      data.items.forEach(function(item: any) {
        list.push({"title": item.name})
      });
      setOfficers(list);
      setShowDirectors(true);
    }
  }

  function getShareholders(data: companyOfficers) {
    let list: Array<companyOfficerListItem> = [];

    if (data.items.length >=1) {
      data.items.forEach(function(item: any) {
        list.push({"title": item.name})
      });
      setShareholders(list);
      setShowShareholders(true);
    }
  }

  return (
    <div className="center-wrapping-container company-profile-section">
      <h1>{companyProfile.title}</h1>

        <h3>Incorporation date:</h3>
        {companyProfile.date_of_creation}

        <h3>Registered address:</h3>
        {companyProfile.address_snippet}

        <h3>Company directors:</h3>
        {!showDirectors && (
          <span>Sorry, no directors are listed.</span>
        )}
        {showDirectors && (
          <List
            itemLayout="horizontal"
            dataSource={officers}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar src="avatar.png" />}
                  title={item.title}
                  description="Description"
                />
              </List.Item>
            )}
          />
        )}

        <h3>Shareholders:</h3>
        {!showShareholders && (
          <span>Sorry, no shareholders are listed.</span>
        )}
        {showShareholders && (
          <List
            itemLayout="horizontal"
            dataSource={shareholders}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar src="avatar.png" />}
                  title={item.title}
                  description="Description"
                />
              </List.Item>
            )}
          />
        )}

        <h3>Group structure:</h3>
        <span>Sorry, no group structure information is available.</span>

    </div>
  )
}

export default CompanyProfile;
