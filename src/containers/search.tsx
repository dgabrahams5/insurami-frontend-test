import React from "react";
import { useEffect, useState } from 'react';
import '../App.css';


export type AppProps = {
  setItems: any;
};

const SearchBar: React.FC<AppProps> = ({
  setItems
}) => {
  const [noResults, setNoResults] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState<string>('');
  let list: Array<string> = [];

  const getAPI = () => {
    let username = "05b3fea5-3cec-4f66-9e03-9e78a18aacf6";
    let password = "";
    const url = 'https://api.company-information.service.gov.uk/search/companies?q=' + searchTerm;

    let headers = {
      'Authorization': 'Basic ' + btoa(username + ":" + password),
      'Accept': 'text/html',
    }

    fetch(url, {
      headers: headers,
    })
    .then((res)=>{ 
            if(res.ok) return res.json(); 
            else throw new Error("Status code error :" + res.status) 
    })
    .then((data) => {
      if (data.items.length > 0) {
        setNoResults(false);
        data.items.forEach(function(item: string){list.push(item)});
        setItems(list);
      } else {
        setNoResults(true);
      }
    })
    .catch(error=>console.log(error))
  };

  function handleSubmit(ev: React.FormEvent<HTMLFormElement>): void {
    ev.preventDefault();
    const formElements = (ev.target as HTMLFormElement).elements;
    const searchTerm = (formElements[0] as HTMLInputElement).value;
    setSearchTerm(searchTerm);
  }

  useEffect(() => {
    if (searchTerm.length > 0) {
      getAPI();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm]);

  return (
    <>
      <form
        onSubmit={handleSubmit}
        className="center-wrapping-container search-form"
      >
        <label htmlFor="searchbar" className="form-search-label">Type a company name to search:</label>
        <input type="text" id="search" name="search" />
        <input type="submit" value="Submit" />

        {noResults && (
          <div className="no-results-container">
            No results, please try again.
          </div>
        )}
      </form>
    </>
  )
}
   
export default SearchBar;
