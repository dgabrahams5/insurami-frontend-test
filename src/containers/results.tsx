import React from "react";
import { List } from 'antd';
import { useEffect } from 'react';
import '../App.css';
import { companyProfile } from '../App';

export type AppProps = {
  items: Array<companyProfile>;
  setcompanyProfile: any;
  setLoadProfile: any;
};

const Results: React.FC<AppProps> = ({
  items,
  setcompanyProfile,
  setLoadProfile,
}) => {

  useEffect(() => {
    items.forEach(function(item: any, index: number){
        item.index = index
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleClick(ev: React.MouseEvent<HTMLButtonElement, MouseEvent>, index: number): any {
    setcompanyProfile(items[index]);
    setLoadProfile(true);
  }

  return (
    <div className="center-wrapping-container results-list-container">
      <List
        itemLayout="horizontal"
        dataSource={items}
        renderItem={item => (
          <List.Item>
            <button type="button" className="results-li-button" onClick={(e) => handleClick(e, item.index)}>SELECT</button>
            <List.Item.Meta
              title={item.title}
              description={"Company number: " + item.company_number}
            />
          </List.Item>
        )}
      />
    </div>
  )
   
}

export default Results;
