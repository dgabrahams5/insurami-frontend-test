import React from 'react';
import PropTypes from 'prop-types';

export type ButtonProps = {
  type?: 'button' | 'submit' | 'reset';
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  children: React.ReactNode;
  disabled?: boolean;
  className?: string;
  index?: number;
};

// const ButtonRender: React.RefForwardingComponent<
const Button: React.RefForwardingComponent<
  HTMLButtonElement,
  ButtonProps
  // eslint-disable-next-line react/prop-types
> = ({ type, onClick, className, children, disabled, index }, ref) => {
  return (
    <>
      <button
        /* eslint-disable-next-line react/button-has-type */
        type={type}
        className={className}
        onClick={onClick}
        disabled={disabled}
        ref={ref}
        // index={index}
      >
        {children}
      </button>
    </>
  );
};

// export const Button = React.forwardRef(ButtonRender);

// export default ButtonRender;
export default Button;

// Button.defaultProps = {
//   type: 'button',
//   onClick: undefined,
//   disabled: false,
//   className: undefined,
// };

// Button.propTypes = {
//   onClick: PropTypes.func,
//   children: PropTypes.node.isRequired,
//   type: PropTypes.oneOf(['button', 'submit', 'reset']),
//   disabled: PropTypes.bool,
//   className: PropTypes.string,
// };
