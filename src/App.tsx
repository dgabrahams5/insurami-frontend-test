import React from "react";
import { useState } from 'react';
import { Layout } from 'antd';
import './App.css';

import SearchBar from './containers/search';
import Results from './containers/results';
import CompanyProfile from './containers/company_profile';

export interface companyProfile {
  address: object;
  address_snippet: string;
  company_number: number;
  company_status: string;
  company_type: string;
  date_of_creation: string;
  description: string;
  description_identifier: Array<string>;
  kind: string;
  links: object;
  matches: object;
  snippet: string;
  title: string;
  index: number;
}

const App: React.FC = () => {

  const { Header, Footer, Content } = Layout;
  const [loadProfile, setLoadProfile] = useState<boolean>(false);
  const [companyProfile, setcompanyProfile] = useState<companyProfile>();
  const [items, setItems] = useState<Array<companyProfile>>([]);

  return (
    <>

      <Layout>
        <Header>
          {items.length > 0 && (
            <a href="/">Back to search</a>
          )}
        </Header>
        <Content>
          {items.length === 0 && (
            <SearchBar
              setItems={setItems}
            />
          )}
          {!loadProfile && items.length > 0 && (
            <Results
              items={items}
              setcompanyProfile={setcompanyProfile}
              setLoadProfile={setLoadProfile}
            />
          )}
          {loadProfile && (
            <CompanyProfile
              companyProfile={companyProfile}
            />
          )}
        </Content>
        <Footer></Footer>
      </Layout>

    </>
  )
}
   
export default App;
