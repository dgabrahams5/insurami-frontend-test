# Frontend test #

Write a web application that can search for company profiles using the Companies House API
and can display basic information.

You can use **any JS framework you like**. We use Mobx/MST.

You do not need to implement a backend unless you go for the bonus. If you do, you can use
**any Python web framework you want**. We use FastAPI.

You can take as much time as you like to complete this test. It is a great opportunity to show us how awesome you are 
at what you do. However, **we do not want you to take more than 1 day** on it.
If you run out of time or you choose to only work a couple of hours that is OK, but **please prioritise what you work on**. 
We would like to see this reflected as timed increments of your work in the commits of your branch.

### User journey ###

The user lands on an empty page with a search box. Upon entering the search criteria and
hit 'enter' a list of results should be rendered containing the company name and registration number, along
with a "Select" button. If clicked on the "Select" button, the UI navigates to the company profile page, where
more information is displayed, including:

* Incorporation date
* Registered address
* Company directors
* Shareholders
* Group structure

### Goals ###

We'd like you to deploy a production version of the app so anyone can play with it.

### Bonus ###

* Persisting company profiles
* Pesisting company searches
* User Authentication
* Other features that can be of value to the user
* Handle rate limits
* Other architectural improvements you may think of

### What we will be looking at ###

* API integration & State management
* Edge cases - handling nulls
* Tests
* UI design
* Documentation, Source control good practices

### Things you will need to get started ###

1. Go to the (Companies House website)[https://developer.company-information.service.gov.uk/] and open an account to start using the API.
2. Fork this repo.
3. Make the first commit to set the start time.

### Build Results ###

To view build results please open the following URL in a browser:
[Production Server](http://ec2-107-21-87-49.compute-1.amazonaws.com)

The pipeline runs through a build and then deploys via FTP to a custom Amazon AWS EC2 instance.
